package modelare;

import javax.print.DocFlavor;

public class Person {
    private String id;
    private String nume;
    private String prenume;
    private int age;
    private String gender;
    private boolean pensionar;
    Person(String id, String nume, String prenume, int age, String gender, boolean pensionar) {
        this.id = id;
        this.nume=nume;
        this.prenume=prenume;
        this.age=age;
        this.gender=gender;
        this.pensionar=pensionar;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id=id;
    }
    public String getNume(){
        return nume;
    }
    public void setNume(String nume) {
        this.nume=nume;
    }
    public String getPrenume(){
        return prenume;
    }
    public void setPrenume(String prenume) {
        this.prenume=prenume;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age=age;
    }
    public String getGender(){
        return gender;
    }
    public void setGender(String gender){
        this.gender=gender;
    }
    public boolean isPensionar() {
        return pensionar;
    }
    public void setisPensionar(boolean pensionar ) {
        this.pensionar=pensionar;
    }

}
