package numbers;

import first.Test;

public class TestNumber {

    private TestNumber() {
    }

    public static double computeSum(Number[] numbers) {
      double sum = 0;
      for(Number nr:numbers) {
      sum+=nr.doubleValue();

    } return sum;
    }
    // supraincarcare metodei computeSum
    public static int computeSum(Integer[] numbers) {
        int sum = 0;
        for(Integer nr:numbers) {
            sum += nr;

        } return sum;
    }
    public static int computeSum(String numbers, String delimiter){
        String [] nrs = numbers.split("delimiter");
        Integer [] intNumbers = new Integer[nrs.length];
        for(int i=0; i<nrs.length; i++) {
            String currentNr = nrs[i];
            intNumbers[i]=Integer.parseInt(currentNr);
        } return computeSum(intNumbers);


    }


}
