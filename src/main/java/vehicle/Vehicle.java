package vehicle;

public class Vehicle {
    protected int maxSpeed;

    public Vehicle(){
        this(50);
    }
    Vehicle(int maxSpeed) {
        this.maxSpeed=maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

}
